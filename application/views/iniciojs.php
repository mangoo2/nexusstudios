<!--<script src="<?php echo base_url();?>public/plugins/simplelightbox-master/dist/simple-lightbox.js?v2.10.3"></script>
<link rel="stylesheet" href="<?php echo base_url();?>public/plugins/simplelightbox-master/dist/simple-lightbox.css?v2.10.3">-->

<script src="<?php echo base_url();?>public/plugins/lightbox2/lightbox.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>public/plugins/lightbox2/lightbox.min.css">



<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loading/jquery.loading.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loading/demo.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/inicio.js?v=<?php echo date('Ymdgis')?>"></script>
<style type="text/css">
	.loadingcambiodidioma{
		font-size: 3.15em !important;
	}
	.loadingcambiodidioma span{
		cursor: pointer;
	}
</style>
<script type="text/javascript">
	$(document).ready(function($) {
		<?php if(!$this->session->userdata('idioma')){ ?>
		$('.videoinicio').loading(
			{
				theme: 'dark',
				message: '<span class="loadingcambiodidioma"><span onclick="idioma(0)">EN</span> | <span onclick="idioma(1)">ES<span><span>'
			});
		<?php } ?>
	});

	function mostrar_i(){
		$('.inicio_video').css('display','block');
		$('#nosotros').css('display','none');
		$('#publicidad').css('display','none');
		$('#fotografia').css('display','none');
		$('#cine').css('display','none');
		$('#contacto').css('display','none');
		$('.home-content').css('margin-top','56.25vw');
		$('.home-content').css('padding-top','16px');
		ocultar_menu();
	}
    
	function mostrar_n(){
		$('.inicio_video').css('display','none');
		$('#nosotros').css('display','block');
		$('#publicidad').css('display','none');
		$('#fotografia').css('display','none');
		$('#cine').css('display','none');
		$('#contacto').css('display','none');
		$('.site-footer').css('display','none');
		$('.home-content').css('margin-top','0vw');
		$('.home-content').css('padding-top','0px');
		ocultar_menu();
	}

	function mostrar_pu(){
		$('.inicio_video').css('display','none');
		$('#nosotros').css('display','none');
		$('#publicidad').css('display','block');
		$('#fotografia').css('display','none');
		$('#cine').css('display','none');
		$('#contacto').css('display','none');
		$('.site-footer').css('display','none');
		$('.home-content').css('margin-top','0vw');
		$('.home-content').css('padding-top','0px');
		ocultar_menu();
	}

	function mostrar_fo(){
		$('.inicio_video').css('display','none');
		$('#nosotros').css('display','none');
		$('#publicidad').css('display','none');
		$('#fotografia').css('display','block');
		$('#cine').css('display','none');
		$('#contacto').css('display','none');
		$('.home-content').css('margin-top','0vw');
		$('.home-content').css('padding-top','0px');
		ocultar_menu();
	}

	function mostrar_cine(){
		$('.inicio_video').css('display','none');
		$('#nosotros').css('display','none');
		$('#publicidad').css('display','none');
		$('#fotografia').css('display','none');
		$('#cine').css('display','block');
		$('#contacto').css('display','none');
		$('.site-footer').css('display','none');
		$('.home-content').css('margin-top','0vw');
		$('.home-content').css('padding-top','0px');
		ocultar_menu();
	}

	function mostrar_contacto(){
		$('.inicio_video').css('display','none');
		$('#nosotros').css('display','none');
		$('#publicidad').css('display','none');
		$('#fotografia').css('display','none');
		$('#cine').css('display','none');
		$('#contacto').css('display','block');
		$('.site-footer').css('display','block');
		$('.home-content').css('margin-top','0vw');
		$('.home-content').css('padding-top','0px');
		ocultar_menu();
	}

	function ocultar_menu(){
		$('.id_menu').removeClass('js region- no-hiddenscroll objectfit object-fit navigation-active');
		$('.id_menu').addClass('js region- no-hiddenscroll objectfit object-fit');
		$('.page-id-76').css('padding-right','0px');
		$('.menu-button').css('transform','none');
	}

	function video_ver(num){
		$('#modal_video').modal('show');
		if(num==1){
            $('.video_num').html('<iframe width="760" height="515" src="https://www.youtube.com/embed/zi4JzdJ-3iA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
		}else if(num==2){
            $('.video_num').html('<iframe width="760" height="515" src="https://www.youtube.com/embed/h6Ki2EjxLOw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
        }else if(num==3){
            $('.video_num').html('<iframe width="760" height="515" src="https://www.youtube.com/embed/KVyO5nLBJG0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
        }else if(num==4){
            $('.video_num').html('<iframe width="760" height="515" src="https://www.youtube.com/embed/xxPMl5N5ZYE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
        }else if(num==5){
            $('.video_num').html('<iframe width="760" height="515" src="https://www.youtube.com/embed/zMdL6CMuvYM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
        }else if(num==6){
            $('.video_num').html('<iframe width="760" height="515" src="https://www.youtube.com/embed/caq6y9F8I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
        }else if(num==7){
            $('.video_num').html('<iframe width="760" height="515" src="https://www.youtube.com/embed/JVGrKseigaQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
        }else if(num==8){
            $('.video_num').html('<iframe width="760" height="515" src="https://www.youtube.com/embed/hI4Zj2-nd34" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
        }else if(num==9){
            $('.video_num').html('<iframe width="760" height="515" src="https://www.youtube.com/embed/YrBK6-iQ6Uo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
        }else if(num==10){
            $('.video_num').html('<iframe width="760" height="515" src="https://www.youtube.com/embed/kIvSYCwjXXA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
        }else if(num==11){
            $('.video_num').html('<iframe width="760" height="515" src="https://www.youtube.com/embed/UZRcp10Tfxk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
        }else if(num==12){
            $('.video_num').html('<iframe width="760" height="515" src="https://www.youtube.com/embed/q3r3BPVMYEc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
        }else if(num==13){
            $('.video_num').html('<iframe width="760" height="515" src="https://www.youtube.com/embed/WdMA-z1egyE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
        }
        
		$('#modal_video').on('hidden.bs.modal', function () {
  			$('.video_num').html('');
		})
		
	}
    
    var cont=1;
    

	function anterior(){
		cont--;
		if(cont==1){
			$('.btn_anterior').css('display','none');
            $('.modelos_1').show(1500);
		    $('.modelos_2').hide(1500);
		    $('.modelos_3').hide(1500);
		    $('.modelos_4').hide(1500);
		}else if(cont==2){
			$('.btn_anterior').css('display','block');
            $('.modelos_1').hide(1500);
		    $('.modelos_2').show(1500);
		    $('.modelos_3').hide(1500);
		    $('.modelos_4').hide(1500);
		}else if(cont==3){
            $('.modelos_1').hide(1500);
		    $('.modelos_2').hide(1500);
		    $('.modelos_3').show(1500);
		    $('.modelos_4').hide(1500);
		    $('.btn_siguiente').css('display','block');
		}else if(cont==4){
            $('.modelos_1').hide(1500);
		    $('.modelos_2').hide(1500);
		    $('.modelos_3').hide(1500);
		    $('.modelos_4').show(1500);
		    $('.btn_siguiente').css('display','none');
		}
	}

	function siguiente(){
		cont++;
        if(cont==1){
        	$('.btn_anterior').css('display','none');
            $('.modelos_1').show(1500);
		    $('.modelos_2').hide(1500);
		    $('.modelos_3').hide(1500);
		    $('.modelos_4').hide(1500);
		}else if(cont==2){
			$('.btn_anterior').css('display','block');
            $('.modelos_1').hide(1500);
		    $('.modelos_2').show(1500);
		    $('.modelos_3').hide(1500);
		    $('.modelos_4').hide(1500);
		}else if(cont==3){
            $('.modelos_1').hide(1500);
		    $('.modelos_2').hide(1500);
		    $('.modelos_3').show(1500);
		    $('.modelos_4').hide(1500);
		    $('.btn_siguiente').css('display','block');
		}else if(cont==4){
            $('.modelos_1').hide(1500);
		    $('.modelos_2').hide(1500);
		    $('.modelos_3').hide(1500);
		    $('.modelos_4').show(1500);
		    $('.btn_siguiente').css('display','none');
		}
	}

</script>