<!DOCTYPE html>
<html lang="en-us" class="id_menu no-js region-">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i&amp;display=swap&amp;subset=latin-ext" rel="stylesheet">
		<script>var w_areas = [ "global","us" ];</script>
		<title>REDAPOL</title>
		<meta name='robots' content='max-image-preview:large' />
		<link rel="canonical" href="index.html" />
		<meta name="description" content="Nexus produces Film, Animation, Interactive and Original Content." />
		<meta name="robots" content="noodp,noydir" />
		<meta property="og:title" content="REDAPOL" />
		<meta property="og:type" content="website" />
		<meta property="og:description" content="REDAPOL" />
		<meta property="og:url" content="https://https://redapol.com/" />
		<meta property="og:locale" content="es" />
		<meta property="og:site_name" content="REDAPOL" />
		<meta property="twitter:card" content="summary" />
		<meta property="twitter:site" content="REDAPOL" />
		<meta property="twitter:title" content="REDAPOL" />
		<meta property="twitter:description" content="REDAPOL." />
		<link rel='stylesheet' id='wp-block-library-css' href='wp-includes/css/dist/block-library/style.min69c8.css?ver=5.8.4' type='text/css' media='all' />
		<link rel='stylesheet' id='style-css'  href='<?php echo base_url();?>app/themes/nexus/css/stylee08c.css?v=v1.1.2&amp;ver=5.8.4' type='text/css' media='all' />
		<script type='text/javascript' src='https://code.jquery.com/jquery-3.6.0.min.js' id='jquery-js'></script>
		<script type='text/javascript' src='<?php echo base_url();?>player.vimeo.com/api/player.js' id='vimeo-player-js'></script>
		<script type='text/javascript' src='app/themes/nexus/js/vendorf26c.js?v=v1.1.2' id='vendor-js'></script>
		<script type='text/javascript' src='app/themes/nexus/js/scriptf26c.js?v=v1.1.2' id='mainscript-js'></script>
		<link rel="canonical" href="index.html" />
		<link rel='shortlink' href='index.html' />
		<link rel="icon" type="image/png" href="<?php echo base_url();?>public/img/LOGO-CIRCULO-NEGRO-50x50.png" sizes="50x50" />
		<link rel="icon" type="image/png" href="<?php echo base_url();?>LOGO-CIRCULO-NEGRO-150x150.png" sizes="150x150" />
		<link rel="shortcut icon" href="<?php echo base_url();?>public/img/LOGO-CIRCULO-NEGRO-50x50.png">
		<link rel="apple-touch-icon" href="<?php echo base_url();?>LOGO-CIRCULO-NEGRO-150x150.png" />
		<meta name="msapplication-TileImage" content="<?php echo base_url();?>LOGO-CIRCULO-NEGRO-150x150.png" />

		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

		<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy" crossorigin="anonymous"></script>



		<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/bootstrap.min.css">
		<script type='text/javascript' src='<?php echo base_url();?>public/js/bootstrap.bundle.min.js'></script>
		<script type='text/javascript' src='<?php echo base_url();?>public/js/bootstrap.min.js'></script>-->
		<style type="text/css">
			.home-header-block .heading .logomark {
			      
			      background-image: url("<?php echo base_url();?>public/img/L001.png");
			      background-repeat: no-repeat;
			      background-size: 100% 100%; 
			}
			.site-logo.inverted{
				background-image: url("<?php echo base_url();?>public/img/REDAPOL-LOGO-NEGRO-SS-260x125.png");
			}
			.site-logo{
				background-image: url("<?php echo base_url();?>public/img/REDAPOL-LOGO-NEGRO-SS-260x125.png");	
			}
            a:hover {
			    color: red;
			}
            .margen{
            	width: 48%; float: left; margin: 10px; border: 0;
            }
            .letra_n{
            	font-size: 38px;
            }
            .flecha_izq{
                position: absolute;top: 300px;left: 41px; display: none;
            }
            .flecha_der{
            	position: absolute;top: 300px;right: 70px;
            }
            .flecha_img{
                width: 130px;
            }
            .margen_t{
                width: 47%;
            }
            .input_t{
            	border-bottom: 2px solid #F9F8F8 !important;
            }
            .placeholder_s{
            	font-family: Arial;
			    font-size: 20px;
			    font-weight: bold;
            }
            .textarea_t{
            	width: 100%;
			    border: none;
			    background-color: transparent;
			    height: 63px;
			    color: white;
            }

            .boton{
            	border: 3px solid white;
            	background: black;
			    color: white;
			    border-radius: 9px;
			    width: 100%;
            }
         
			html, body 
			{ 
			 height: 100%;
			 overflow: hidden
			}
			
			@media screen and (min-width: 1600px){
				.page-content, .site-footer {
				    padding-left: 0rem;
				    padding-right: 0rem;
				}
            }

            @media screen and (min-width: 992px){
				.home-content {
				    padding-top: 0px;
				}
			}
			body {
			    margin: 0;
			    font-family: var(--bs-body-font-family);
			    font-size: var(--bs-body-font-size);
			    font-weight: var(--bs-body-font-weight);
			    line-height: var(--bs-body-line-height);
			    color: var(--bs-body-color);
			    text-align: var(--bs-body-text-align);
			    background-color: #000;
			    -webkit-text-size-adjust: 100%;
			    -webkit-tap-highlight-color: transparent;
			}
            
            @media screen and (max-width: 480px) {
            	.margen{
            	    width: 100%;
				    float: left;
				    margin: 0px;
				    border: 0;
				    padding: 8px;
                    background: black;	
            	}
            	.letra_n{
	            	font-size: 30px;
	            }
	            .video_n{
	            	position: absolute;
				    width: 100%;
				    height: 100%;
				    -o-object-fit: cover;
				    object-fit: cover;
	            }
	            .contacto_margen{
	            	padding: 13px;
	            }
	            .flecha_izq{
	                top: 327px;
	                left: 287px;
	            }
	            .flecha_der{
	            	top: 248px;
                    right: 30px;
	            }
	            .flecha_img{
	                width: 40px;
	            }
	            .margen_t{
	                width: 75%;
	            }
	            .br_none{
	            	display: none;
	            }
            } 
			    
		</style>
	</head>
<body class="home page-template page-template-tpl-homepage page-template-tpl-homepage-php page page-id-76">
	<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url()?>">
	<section id="inicio" class="inicio"></section>
	<div class="menu-button">
		<div class="line A-line" style="background: red;"></div>
		<div class="line B-line" style="background: red;"></div>
	</div>
	<header class="site-header">
		<div class="logo-menu">
			<h1 class="heading site-logo">
			<a href="index.html">
				<span class="hidden-text"></span>
			</a>
			</h1>
			<div class="site-logo inverted">
				<a href="index.html"></a>
			</div>
			<ul class="main-menu">
				<!--<li class="">
					<a href="film-and-episodic/index.html"><?php echo $textoesen['menu1'];?></a>
				</li>-->
				<li class=""><a href="#inicio"><img style="width: 140px;" src="<?php echo base_url();?>public/img/L001.png"></a><br></li>
				<li class=""><a href="#nosotros">Nosotros</a></li>
				<li class=""><a href="#publicidad">Publicidad</a></li>
				<li class=""><a href="#proyectos">Fotografía</a></li>
				<li class=""><a href="#proyectos">Cine</a></li>
				<!--<li class=""><a href="#proyectos">Proyectos</a></li>-->
				<!--<li class=""><a href="talent/index.html">Noticias</a></li>-->
				<li class=""><a href="talent/index.html">Contacto</a></li>
			</ul>
		</div>
		<nav class="site-navigation scroll-container">
			<div class="body scroll-content">
				<ul class="items primary" style="line-height: 46px; font-size: 30px;">
					<!--<li class=""><a href="film-and-episodic/index.html"><?php echo $textoesen['menu1'];?></a></li>-->
					<li class=""><a href="#inicio"  onclick="mostrar_i()"><img style="width: 140px;" src="<?php echo base_url();?>public/img/L001.png"></a><br></li>
					<li class=""><a href="#nosotros" onclick="mostrar_n()">Nosotros</a></li>
					<li class=""><a href="#publicidad" onclick="mostrar_pu()">Publicidad</a></li>
					<li class=""><a href="#fotografia" onclick="mostrar_fo()">Fotografía</a></li>
					<li class=""><a href="#cine" onclick="mostrar_cine()">Cine</a></li>
					<!--<li class=""><a href="#proyectos">Proyectos</a></li>-->
					<!--<li class=""><a href="talent/index.html">Noticias</a></li>-->
					<li class=""><a href="#contacto" onclick="mostrar_contacto()">Contacto</a></li>

				</ul>
				<!--
				<ul class="items primary">
					<li class=""><a href="#audiovisual">Trabajos audiovisual</a></li>
					<li class=""><a href="#fotografia">Trabajos fotografia</a></li>
					<li class=""><a href="#servicios">Servicios</a></li>
				</ul>
			    -->
			</div>
		</nav>
	</header>
	<div class="site-content">
		<header class="home-header-block inicio_video">
			<h1 class="heading">
				<span class="logomark"></span>
				<span class="logotype" style="display: none;">
					<span class="newline">redapol</span> 
				</span>
			</h1>
			<div id="inicio" class="body">
				<video loop autoplay muted playsinline  class="videoinicio">
					<!--<source src="https://player.vimeo.com/external/500904152.sd.mp4?s=07c66ab013362bba5b50695f2a0c2c0f0d63a8f6&amp;profile_id=165" type="video/mp4">-->
					<source src="<?php echo base_url();?>public/video/reflejo.mp4" type="video/mp4">
				</video>
			</div>
		</header>
		<article class="home-content page-content">
			<div class="logo-menu">
				<h1 class="heading site-logo"><a href="index.html"><span class="hidden-text">REDAPOL</span></a></h1>
				<div class="site-logo inverted"><a href="index.html"></a></div>
				<ul class="main-menu">
					<!--<li class=""><a href="film-and-episodic/index.html"><?php echo $textoesen['menu1'];?></a></li>-->
					<li class=""><a href="#inicio"><img style="width: 140px;" src="<?php echo base_url();?>public/img/L001.png"></a><br></li>
					<li class=""><a href="#nosotros">Nosotros</a></li>
					<li class=""><a href="#publicidad">Publicidad</a></li>
					<li class=""><a href="#fotografia">Fotografía</a></li>
					<li class=""><a href="#cine">Cine</a></li>
					<!--<li class=""><a href="#proyectos">Proyectos</a></li>-->
					<!--<li class=""><a href="talent/index.html">Noticias</a></li>-->
					<li class=""><a href="#contacto">Contacto</a></li>
				</ul>
			</div>
			<div class="util-fader"></div>
			<div class="util-pusher"></div>
			<div class="util-marker"></div>
			
			<section id="nosotros" class="nosotros"  style="display: none; display: flex; flex-direction: column; height: 100vh;">
					<div class="video1 video_n">
						<video class="video_n" loop autoplay muted playsinline  style="width:100%" id="video1">
							<source src="<?php echo base_url();?>public/video/Video.mp4" type="video/mp4">
						</video>	
					</div>
 					
					<div class="container" style="padding: 0px;width: 100%;margin: 0px;">
						<div class="row" style="background: url(<?php echo base_url();?>public/img/L001.png) #ffffff6e; background-size: 100px; background-repeat: no-repeat; background-position-x: 5%; background-position-y: bottom; height: 100%; width: 103%;">
							<div class="col-md-12" style="padding-left: 10%;padding-right: 10%;">
								<br>
								<h2 class="text-dark">Nosotros</h2>		
								<br><br>
								<p class="text-danger letra_n" style="text-align: justify;">Cuando la creatividad, la dedicación, el talento y la pasión se unen; se crea una obra maestra. </p>
								<p class="text-danger letra_n">En REDAPOL llevamos 5 años haciendo lo que amamos.</p>
							</div>
						</div>
					</div>
			</section>
			<section id="publicidad" class="publicidad" style="background: black; display: none; flex; flex-direction: column; height: 100vh; overflow-x: hidden;">
				<div class="container" >
					<div class="row">
						<div class="col-md-12" style="text-align:center;">
							<h2 class="text-white">Publicidad</h2>		
						</div>
						<div class="col-md-1">
							<a class="btn_anterior flecha_izq" onclick="anterior()"><img class="flecha_img" src="<?php echo base_url()?>public/img/ra.svg"></a>
						</div>	
						<div class="col-md-10">
							<div class="row">
								<div class="col-md-12 modelos_1">
									<div class="card cardprocyecto margen_t" onclick="video_ver(1);" style="cursor: pointer; float: left; margin: 10px; border: 0; background: url(<?php echo base_url().'public/img/1.png' ?>); background-size:cover;">
										<div class="card-body" style="height: 166px; background: #00000063;"></div>
									  <div class="card-body" style="background: #00000063;">
									  	<p class="text-white" style="margin:0px">CAP 3 BOSCH HD</p>
		                                <br>
									  </div>
									</div>
									<div class="card cardprocyecto margen_t" onclick="video_ver(2);" style="float: left; margin: 10px; border: 0; background: url(<?php echo base_url().'public/img/2.png' ?>); background-size:cover;">
										<div class="card-body" style="height: 166px; background: #00000063;"></div>
									  <div class="card-body" style="background: #00000063;">
									  	<p class="text-white" style="margin:0px">VIDEO MC CORMICK</p>
									    <br>
									  </div>
									</div>
									<div class="card cardprocyecto margen_t" onclick="video_ver(3);" style="float: left; margin: 10px; border: 0; background: url(<?php echo base_url().'public/img/3.png' ?>); background-size:cover;">
										<div class="card-body" style="height: 166px; background: #00000063;"></div>
									  <div class="card-body" style="background: #00000063;">
									  	<p class="text-white" style="margin:0px">Motion Graphics Spot Corpus Lex</p>
									    <br>
									  </div>
									</div>
									<div class="card cardprocyecto margen_t" onclick="video_ver(4);" style="float: left; margin: 10px; border: 0; background: url(<?php echo base_url().'public/img/4.png' ?>); background-size:cover;">
										<div class="card-body" style="height: 166px; background: #00000063;"></div>
									  <div class="card-body" style="background: #00000063;">
									  	<p class="text-white" style="margin:0px">Spot Ahora somos Movistar Plus</p>
									    <br>
									  </div>
									</div>
								</div>
								<div class="col-md-12 modelos_2" style="display: none">
									<div class="card cardprocyecto margen_t" onclick="video_ver(5);" style="cursor: pointer; float: left; margin: 10px; border: 0; background: url(<?php echo base_url().'public/img/5.png' ?>); background-size:cover;">
										<div class="card-body" style="height: 166px; background: #00000063;"></div>
									  <div class="card-body" style="background: #00000063;">
									  	<p class="text-white" style="margin:0px">Sabe a Perú - Movistar - Plus</p>
		                                <br>
									  </div>
									</div>
									<div class="card cardprocyecto margen_t" onclick="video_ver(6);" style="float: left; margin: 10px; border: 0; background: url(<?php echo base_url().'public/img/6.png' ?>); background-size:cover;">
										<div class="card-body" style="height: 166px; background: #00000063;"></div>
									  <div class="card-body" style="background: #00000063;">
									  	<p class="text-white" style="margin:0px">Sabe a Perú - Movistar Plus</p>
									    <br>
									  </div>
									</div>
									<div class="card cardprocyecto margen_t" onclick="video_ver(7);" style="float: left; margin: 10px; border: 0; background: url(<?php echo base_url().'public/img/7.png' ?>); background-size:cover;">
										<div class="card-body" style="height: 166px; background: #00000063;"></div>
									  <div class="card-body" style="background: #00000063;">
									  	<p class="text-white" style="margin:0px">Cerveza Bucanero spot</p>
									    <br><br>
									  </div>
									</div>
									<div class="card cardprocyecto margen_t" onclick="video_ver(8);" style="float: left; margin: 10px; border: 0; background: url(<?php echo base_url().'public/img/8.png' ?>); background-size:cover;">
										<div class="card-body" style="height: 166px; background: #00000063;"></div>
									  <div class="card-body" style="background: #00000063;">
									  	<p class="text-white" style="margin:0px">Los Guatemaltecos tenemos un Don Gasolineras Don Arturo 2016</p>
									    <br>
									  </div>
									</div>
								</div>
								<div class="col-md-12 modelos_3" style="display: none">
									<div class="card cardprocyecto margen_t" onclick="video_ver(9);" style="cursor: pointer; float: left; margin: 10px; border: 0; background: url(<?php echo base_url().'public/img/9.png' ?>); background-size:cover;">
										<div class="card-body" style="height: 166px; background: #00000063;"></div>
									  <div class="card-body" style="background: #00000063;">
									  	<p class="text-white" style="margin:0px">Lanzamiento Malta Carnaval</p>
		                                <br>
									  </div>
									</div>
									<div class="card cardprocyecto margen_t" onclick="video_ver(10);" style="float: left; margin: 10px; border: 0; background: url(<?php echo base_url().'public/img/10.png' ?>); background-size:cover;">
										<div class="card-body" style="height: 166px; background: #00000063;"></div>
									  <div class="card-body" style="background: #00000063;">
									  	<p class="text-white" style="margin:0px">SPOT SASSÓN GUATEMALA</p>
									    <br>
									  </div>
									</div>
									<div class="card cardprocyecto margen_t" onclick="video_ver(11);" style="float: left; margin: 10px; border: 0; background: url(<?php echo base_url().'public/img/11.png' ?>); background-size:cover;">
										<div class="card-body" style="height: 166px; background: #00000063;"></div>
									  <div class="card-body" style="background: #00000063;">
									  	<p class="text-white" style="margin:0px">Della Quinta Nuevas sabores Sopas de verduras y Fideos</p>
									    <br><br>
									  </div>
									</div>
									<div class="card cardprocyecto margen_t" onclick="video_ver(12);" style="float: left; margin: 10px; border: 0; background: url(<?php echo base_url().'public/img/12.png' ?>); background-size:cover;">
										<div class="card-body" style="height: 166px; background: #00000063;"></div>
									  <div class="card-body" style="background: #00000063;">
									  	<p class="text-white" style="margin:0px">SPOT BLACKBIRD ITALIKA</p>
									    <br>
									  </div>
									</div>
								</div>
								<div class="col-md-12 modelos_4" style="display: none">
									<div class="card cardprocyecto margen_t" onclick="video_ver(13);" style="cursor: pointer; float: left; margin: 10px; border: 0; background: url(<?php echo base_url().'public/img/13.png' ?>); background-size:cover;">
										<div class="card-body" style="height: 166px; background: #00000063;"></div>
									  <div class="card-body" style="background: #00000063;">
									  	<p class="text-white" style="margin:0px">SPOT LÍNEA VORTX ITALIKA</p>
		                                <br>
									  </div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-1">
							<a class="btn_siguiente flecha_der" onclick="siguiente()"><img class="flecha_img" src="<?php echo base_url()?>public/img/ra2.svg"></a>	
						</div>		
					</div>
				</div>
			</section>
			<section id="audiovisual" class="audiovisual" style="background: rgb(204, 51, 51); display: none;">
				<div class="container" >
					<div class="row">
						<div class="col-md-12" style="text-align:center;">
							<h2 class="text-white">AUDIO VISUAL</h2>		
						</div>
						<div class="col-md-12">
							<?php foreach ($audiovisual as $item) { ?>
									<div class="card" style="width:  48%; float: left; margin: 10px; border: 0;">
										<iframe width="100%" height="315" src="<?php echo $item['url'];?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
									</div>
							<?php } ?>
						</div>
						
					</div>
					
					
				</div>
			</section>
            <section id="fotografia" class="fotografia" style="background: black; display: none; overflow-y: scroll; height: 1000px;">
				<div class="container" >
					<div class="row">
						<div class="col-md-12" style="text-align:center;">
							<h2 class="text-white">Fotografía</h2>		
						</div>
						<div class="col-md-12">
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/1.jpg">
							</div>
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/4.jpg">
							</div>
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/2.jpg">
							</div>
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/3.jpg">
							</div>
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/5.jpg">
							</div>
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/6.jpg">
							</div>
						</div>
						<div class="col-md-12">	
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/7.jpg">
							</div>
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/8.jpg">
							</div>
						</div>
						<div class="col-md-12">		
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/9.jpg">
							</div>
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/10.jpg">
							</div>
						</div>
						<div class="col-md-12">
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/16.jpg">
							</div>
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/17.jpg">
							</div>
						</div>
						<div class="col-md-12">		
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/12.jpg">
							</div>
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/13.jpg">
							</div>
						</div>
						<div class="col-md-12">		
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/14.jpg">
							</div>
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/15.jpg">
							</div>
						</div>
						<div class="col-md-12">		
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/11.jpg">
							</div>
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/19.png">
							</div>
						</div>
						<div class="col-md-12">		
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/18.png">
							</div>
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/20.png">
							</div>
						</div>
						<div class="col-md-12">		
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/21.png">
							</div>
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/23.png">
							</div>
						</div>
						<div class="col-md-12">		
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/26.png">
							</div>
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/24.png">
							</div>
						</div>
						<div class="col-md-12">		
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/22.png">
							</div>
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/25.png">
							</div>
						</div>
						<div class="col-md-12">		
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/27.png">
							</div>
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/30.png">
							</div>
						</div>
						<div class="col-md-12">		
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/29.png">
							</div>
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/28.png">
							</div>
						</div>
						<div class="col-md-12">		
							<div class="card cardprocyecto margen">
								<img style="border-radius: 10px;" src="<?php echo base_url()?>public/img/fotografia/31.png">
							</div>
						</div>
					</div>
					<br><br><br><br><br><br><br><br><br><br><br><br>	
					<div class="br_none" style="display: none;"><br><br><br><br></div>
				</div>
			</section>
			<section id="cine" class="cine" style="background: black; display: none; display: flex; flex-direction: column; height: 100vh;">
				<div class="container" >
					<div class="row">
						<div class="col-md-12" style="text-align:center;">
							<br><br><br><br><br><br>
							<h2 class="text-white">Próximamente</h2>		
							<br><br><br><br><br><br>
						</div>
					</div>	
				</div>
			</section>
			<section id="servicios" class="servicios" style="background: rgb(204, 51, 51); display: none;">
				<div class="container" >
					<div class="row">
						<div class="col-md-12" style="text-align:center;">
							<h2 class="text-white">Servicios</h2>		
						</div>
						<div class="col-md-12">
							
								<div class="card cardprocyecto" style="width: 48%; float: left; margin: 10px; border: 0; background: url(<?php echo base_url()?>public/img/perceptual-standard.jpg); background-size:cover;" onclick="modal(1)">
								  <div class="card-body" style="height: 266px; background: #00000063;text-align: center;">
								  	<p class="text-white" style="margin:0px">AGENCIA DE TALENTO</p>
								
								  </div>
								</div>

								<div class="card cardprocyecto" style="width: 48%; float: left; margin: 10px; border: 0; background: url(<?php echo base_url()?>public/img/perceptual-standard.jpg); background-size:cover;" onclick="modal(2)">
								  <div class="card-body" style="height: 266px; background: #00000063;text-align: center;">
								  	<p class="text-white" style="margin:0px">TRANSPORTACION</p>
								
								  </div>
								</div>

								<div class="card cardprocyecto" style="width: 48%; float: left; margin: 10px; border: 0; background: url(<?php echo base_url()?>public/img/perceptual-standard.jpg); background-size:cover;" onclick="modal(3)">
								  <div class="card-body" style="height: 266px; background: #00000063;text-align: center;">
								  	<p class="text-white" style="margin:0px">AGENCIA DE LOCACIONES</p>
								
								  </div>
								</div>

								<div class="card cardprocyecto" style="width: 48%; float: left; margin: 10px; border: 0; background: url(<?php echo base_url()?>public/img/perceptual-standard.jpg); background-size:cover;" onclick="modal(4)">
								  <div class="card-body" style="height: 266px; background: #00000063; text-align: center;">
								  	<p class="text-white" style="margin:0px">CATERING</p>
								
								  </div>
								</div>

								
							
						</div>
						
					</div>
					
					
				</div>
			</section>
            <div style="overflow: scroll; height: 400px; flex; flex-direction: column; height: 100vh; overflow-x: hidden;">
				<section id="contacto" class="contacto" style="display: none;">
					<div class="container" >
						<div class="row">
							<div class="col-md-12" style="text-align:center;">
								<br>
								<h2 class="text-black">Contacto</h2>		
							</div>
						</div>	
						<div class="row">
							<div class="col-md-12" style="text-align:center;">
								<h2 class="text-white"></h2>		
							</div>
							<div class="col-md-4 contacto_margen">
								<div style="background: rgb(44, 44, 44); text-align: center; padding-top: 20px; padding-bottom: 50px;">
									<p style="font-size:17px" class="text-white">GUADALAJARA</p>
									<p style="font-size:17px" class="text-white">Justo Sierra #2799, Guadalajara, Jalisco piso 5 oficina 1 C.P  44620</p>
									<a style="font-size:17px" class="text-white" href="tel:5566778900">Conmutador: ( +52) 5566778900</a>
								</div>
							</div>
							<div class="col-md-4 contacto_margen">
								<div style="background: rgb(204, 51, 51); text-align: center; padding-top: 20px; padding-bottom: 50px;">
									<p style="font-size:17px" class="text-white">TIJUANA</p>
									<p style="font-size:17px" class="text-white">Tijuana Misión de San Javier #10644, Tijuana, Baja California piso 8 oficina 1 C.P 22010</p>
									<a style="font-size:17px" class="text-white" href="tel:5510612666">Conmutador: ( +52) 5510612666</a>
								</div>	
							</div>
							<div class="col-md-4 contacto_margen">
								<div style="background: rgb(44, 44, 44); text-align: center; padding-top: 20px; padding-bottom: 50px;">
									<p style="font-size:17px" class="text-white">CDMX</p>
									<p style="font-size:17px" class="text-white">Periferico oriente #2349 Colonia Atlamaya Álbaro Obregon C.P  01760</p>
									<a style="font-size:17px" class="text-white" href="tel:5566778900">Conmutador: ( +52) 5566778900</a>
								</div>
							</div>
							
						</div>
						<div class="row">
							<div class="col-md-12" style="text-align:center;">
								<div class="br_none">
								    <br><br><br>
								</div>
							</div>
						</div>
						
						
					</div>
				</section>

				<footer class="site-footer" style="flex-direction: column;height: 45vh;">
					<div class="row contacto_margen" style="background-color: black;">
						<div class="col-md-2 " style="background-image: url(http://localhost/nexusstudios/public/img/L001.png);min-height: 100px; background-size: 120px; background-position: center; background-repeat: no-repeat;">
						</div>
						<div class="col-md-6">
							<section class="content-block newsletter-block">
							    <h5 class="title">REGISTRATE Y HABLEMOS</h5>
								<input class="email input_t placeholder_s" type="email" id="nombre_t" placeholder="Nombre">
								<input class="email input_t placeholder_s" type="email" id="correo_t" placeholder="Correo">
								<input class="email input_t placeholder_s" type="email" id="telefono_t" placeholder="Teléfono">
								<textarea class="textarea_t input_t placeholder_s" type="email" id="comentario_t" rows="3" placeholder="Comentarios"></textarea>
								<button type="button" class="boton" onclick="enviar_correo()"><span class="text_enviar">Enviar correo</span></button>
						    </section>
						</div>
						<div class="col-md-4">
							<div class="social-links"><ul class="items"><li class="item"><a href="https://www.instagram.com/redapol_mx/" target="_blank"><img src="<?php echo base_url() ?>public/img/redes/1.png"></a></li><li class="item"><a href="https://www.youtube.com/channel/UCw7-ECOF3EqRzXYrc2ql2SA" target="_blank"><img src="<?php echo base_url() ?>public/img/redes/3.png"></a></li><li class="item"><a href="https://www.tiktok.com/@redapolmx" target="_blank"><img src="<?php echo base_url() ?>public/img/redes/4.png"></a></li><li class="item"><a href="https://vimeo.com/redapolfilms" target="_blank"><img src="<?php echo base_url() ?>public/img/redes/8.png"></a></li></ul></div>
						</div>
						<div class="col-md-12">
							<div class="copyright">© <?php echo date('Y') ?> REDAPOL. All Rights Reserved. <a href="#"> Aviso de privacidad</a></div>
						</div>
					</div>
				</footer>
			</div>			



					

				</body>
					<!-- Mirrored from nexusstudios.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 23 May 2022 23:04:00 GMT -->
				</html>
<style>

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 9999999999; /* Sit on top */
  padding-top: 50px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  background-color: #cc3333;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 85%;
}

/* The Close Button */
.closems {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
  text-align: right;
}

.closems:hover,
.closems:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}
</style>

<div id="myModal_1" class="modal modalesss">

  <!-- Modal content -->
  <div class="modal-content">
  	<div class="row">
  		<div class="col-md-10 text-white" >AGENCIA DE TALENTO</div>
  		<div class="col-md-2"><span class="closems" >&times;</span></div>
  	</div>
  	<div class="row">
  		<div class="col-md-12">
  			<div class="card cardprocyecto" style="width: 48%; float: left; margin: 10px; border: 0; background: url(<?php echo base_url()?>public/img/perceptual-standard.jpg); background-size:cover;">
				  <div class="card-body" style="height: 266px; background: #00000063;text-align: center;">
				  	<p class="text-white" style="margin:0px">TRANSPORTACION</p>
				
				  </div>
				</div>
			<div class="card cardprocyecto" style="width: 48%; float: left; margin: 10px; border: 0; background: url(<?php echo base_url()?>public/img/perceptual-standard.jpg); background-size:cover;">
			  	<div class="card-body" style="height: 266px; background: #00000063;text-align: center;">
			  		<p class="text-white" style="margin:0px">TRANSPORTACION</p>
			
			  	</div>
			</div>
  		</div>
  		<div class="col-md-12 text-white">
  			<p style="font-size: 18px;">Redapol cuenta con su propia agencia de talento. Con el fin de simplificarles las tareas a nuestros clientes, nosotros nos en cargamos internamente de todo el proceso de casting y selección del talento. Redapol no subcontrata a ninguna agencia ni agente de casting, encargándose directamente de la contratación de cualquier artista, influencer, modelos y extras.</p>
  		</div>
  	</div>
  </div>

</div>

<div id="myModal_3" class="modal modalesss">

  <!-- Modal content -->
  <div class="modal-content">
  	<div class="row">
  		<div class="col-md-10 text-white" >LOCACIONES</div>
  		<div class="col-md-2"><span class="closems" >&times;</span></div>
  	</div>
  	<div class="row infoclass_modal3" style="overflow: auto;max-height: 509px;">
  		
  	</div>
  </div>

</div>
<div id="myModal_2" class="modal modalesss">

  <!-- Modal content -->
  <div class="modal-content">
  	<div class="row">
  		<div class="col-md-10 text-white" >TRANSPORTACIÓN </div>
  		<div class="col-md-2"><span class="closems" >&times;</span></div>
  	</div>
  	<div class="row">
  		<div class="col-md-12">
  			<div class="card cardprocyecto" style="width: 48%; float: left; margin: 10px; border: 0; background: url(<?php echo base_url()?>public/img/perceptual-standard.jpg); background-size:cover;">
				  <div class="card-body" style="height: 266px; background: #00000063;text-align: center;">
				  	<p class="text-white" style="margin:0px">FOTO CAMIONETA</p>
				
				  </div>
				</div>
			<div class="card cardprocyecto" style="width: 48%; float: left; margin: 10px; border: 0; background: url(<?php echo base_url()?>public/img/perceptual-standard.jpg); background-size:cover;">
			  	<div class="card-body" style="height: 266px; background: #00000063;text-align: center;">
			  		<p class="text-white" style="margin:0px">FOTO CAMIONETA</p>
			
			  	</div>
			</div>
  		</div>
  		<div class="col-md-12 text-white" style="text-align:center;">
  			<p style="font-size: 18px;">Redapol cuenta con su propia flotilla de camionetas; tanto de carga como de personal.</p>
			<p style="font-size: 18px;">En nuestras tres ubicaciones contamos con el servicio de transportación, con choferes locales</p>
  		</div>
  	</div>
  </div>

</div>


<div class="modal" id="modal_videox">
    <div class="modal-dialog modal-lg">
    	<div>
	        <div class="modal-body">
	        	<div class="row">
			        <div class="col-md-12">
			        	<div class="video_num"></div>
			        </div>  
			    </div>
		    </div>
		</div>
    </div>
</div>
<style type="text/css">
	#modal_video .modal-body,#modal_video .col-md-12{
		padding: 0px;
	}
	#modal_video .modal-content{
		background-color: transparent;
		padding: 0px;
		border: 0px;
	}

</style>

<div class="modal" id="modal_video" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      
      <div class="modal-body">
        <div class="row">
	        <div class="col-md-12">
	        	<div class="video_num"></div>
	        </div>  
	    </div>
      </div>
    </div>
  </div>
</div>