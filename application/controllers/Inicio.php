<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloIdiomas');
        if(!$this->session->userdata('idioma')){
			$this->idioma=1;
		}elseif($this->session->userdata('idioma')==0){
			$this->idioma=0;
		}else{
			$this->idioma=1;
		}
    }

	public function index(){
		$result = $this->ModeloIdiomas->idioma($this->idioma);
		$data['textoesen']=$result;

		$this->load->view('inicio',$data);
		$this->load->view('iniciojs');

	}
	function cambioidioma(){
		$idioma = $this->input->post('idioma');//0 ingles 1 español
		$data = array(
                        'idioma' => $idioma
                    );
		$this->session->set_userdata($data);
	}
    

    function enviar(){
    	$nombre = $this->input->post('nombre');
    	$correo = $this->input->post('correo');
    	$telefono = $this->input->post('telefono');
    	$comentario = $this->input->post('comentario');
        //================================
            //cargamos la libreria email
            $this->load->library('email');
            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='mail.redapol.com'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'contacto@redapol.com';

            //Nuestra contraseña
            $config["smtp_pass"] = 'bbRpczbLHHPv';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
     
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('contacto@redapol.com','REDAPOL');

             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
        //======================
        $this->email->to('hapziel.vivanco@redapol.com','contacto');
        //$this->email->bcc('contacto@redapol.com');
        //$this->email->bcc('soporte@mangoo.mx');
        $this->email->bcc('andres@mangoo.mx');
        $asunto='CONTACTO REDAPOL';

      //Definimos el asunto del mensaje
        $this->email->subject($asunto);
         
      //Definimos el mensaje a enviar
      //$this->email->message($body)
        $message  = "<div>
                      <div style='background: black; border: 1px solid #F8F8F8; border-radius: 15px; padding: 25px; width: 60%; margin-left: auto; margin-right: auto; position: absolute; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);
                                      -moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);
                                      box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);'>
                        <table style='width: 100%;'>
                           <thead>
                            <tr>
                                <th colspan='4' align='center'>
                                  <img src='".base_url()."public/img/L001.png' style='width: 250px;'>
                                </th>
                            </tr>
                            <tr>
                              <th>
                                <br>
                              </th>
                            </tr>
                            <tr>
                              <th colspan='4' style='font-family: sans-serif; font-size:23px; color: white; -webkit-print-color-adjust: exact;' align='left'>Nombre: ".$nombre."<span style='font-size:10px;'><br><br></span>
                              </th>
                            </tr>
                            <tr>
                              <th colspan='4' style='font-family: sans-serif; font-size:23px; color: white; -webkit-print-color-adjust: exact;' align='left'>Correo: ".$correo."<span style='font-size:10px;'><br><br></span>
                              </th>
                            </tr>
                            <tr>
                              <th colspan='4' style='font-family: sans-serif; font-size:23px; color: white; -webkit-print-color-adjust: exact;' align='left'>Teléfono: ".$telefono."<span style='font-size:10px;'><br><br></span>
                              </th>
                            </tr>
                            <tr>
                              <th colspan='4' style='font-family: sans-serif; font-size:23px; color: white; -webkit-print-color-adjust: exact;' align='left'>Asunto: ".$comentario." 
                              </th>
                            </tr>
                          </thead>
                      </table>    
                      </div>
                  </div>";


        $this->email->message($message);

        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }
        //==================
    }

    public function correo(){
		$this->load->view('correo');
	}
 
}