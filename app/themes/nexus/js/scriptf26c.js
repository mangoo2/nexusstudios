'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Clock = function () {

    var $clocks;
    var prefixes = ['-ms-', '-webkit-', '-moz-'];

    function init() {
        $clocks = $('.clock-block');
        $clocks.toArray().map($).forEach(function ($el) {
            var $svg = $el.find('svg');
            var utcOffset = $el.data().utcOffset;
            var clock = new Clock($svg, utcOffset);
            clock.start();
            updateAMPM($el, utcOffset);
            setInterval(function () {
                updateAMPM($el, utcOffset);
            }, 10000);
        });
    }

    var Clock = function () {
        function Clock(svgRoot, timeOffsetHours) {
            _classCallCheck(this, Clock);

            this.el = svgRoot;
            this.offset = timeOffsetHours;
            this.seconds = svgRoot.find('.secondhand');
            this.minutes = svgRoot.find('.minutehand');
            this.hours = svgRoot.find('.hourhand');
            this.intervalID;
        }

        _createClass(Clock, [{
            key: 'start',
            value: function start() {
                var _this = this;

                clearInterval(this.intervalID);
                setInterval(function () {
                    requestAnimationFrame(function () {
                        _this.update();
                    });
                }, 1000);
            }
        }, {
            key: 'update',
            value: function update() {
                var date = makeLocalDate(this.offset);
                var hoursAngle = 360 / 12 * date.getHours() + date.getMinutes() / 2;
                var minuteAngle = 360 * date.getMinutes() / 60;
                var secAngle = 360 * date.getSeconds() / 60;

                this.hours.attr('transform', makeSVGTransformFunction(hoursAngle));
                this.minutes.attr('transform', makeSVGTransformFunction(minuteAngle));
                this.seconds.attr('transform', makeSVGTransformFunction(secAngle));
            }
        }, {
            key: 'stop',
            value: function stop() {
                clearInterval(this.intervalID);
            }
        }]);

        return Clock;
    }();

    function makeSVGTransformFunction(deg) {
        return 'rotate(' + deg + ')';
    }

    function makeLocalDate(timeOffsetHours) {
        var date = new Date(convertDateToUTC(new Date()));
        var dst = date.dst() ? 1 : 0;
        date = new Date(date.setHours(date.getHours() + timeOffsetHours + dst));
        return date;
    }

    function updateAMPM($el, timeOffsetHours) {
        var date = makeLocalDate(timeOffsetHours);
        var $text = $el.find('.am-pm');
        $text.text(date.getHours() > 12 ? 'PM' : 'AM');
    }

    function convertDateToUTC(date) {
        return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
    }

    return {
        init: init
    };
}();

Date.prototype.stdTimezoneOffset = function () {
    var jan = new Date(this.getFullYear(), 0, 1);
    var jul = new Date(this.getFullYear(), 6, 1);
    return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
};

Date.prototype.dst = function () {
    return this.getTimezoneOffset() < this.stdTimezoneOffset();
};
'use strict';

var LookingFor = function () {

  function init() {
    this.el = document.querySelector('.looking-for__wrapper');
    if (this.el) {
      this.selects = this.el.querySelectorAll('.wrapper-select>select');
      this.buttonGo = document.querySelector('#go');

      this.buttonGo.addEventListener('click', function () {
        var select = document.querySelector('select[name="type"]');
        window.location = select.value;
      }.bind(this));
    } else {
      this.selects = [];
    }
    var a, b, c, select, x, i, j, selElmnt;

    for (i = 0; i < this.selects.length; i++) {
      selElmnt = this.selects[i];
      x = selElmnt.parentNode;

      if (selElmnt.selectedIndex === -1) {
        return;
      }

      a = document.createElement("DIV");
      a.setAttribute("class", "select-selected");
      a.innerHTML = "<span>" + selElmnt.options[selElmnt.selectedIndex].innerHTML + "</span>";
      x.appendChild(a);
      /*for each element, create a new DIV that will contain the option list:*/
      b = document.createElement("DIV");
      b.setAttribute("class", "select-items select-hide");
      for (j = 0; j < selElmnt.length; j++) {
        /*for each option in the original select element,
        create a new DIV that will act as an option item:*/
        c = document.createElement("DIV");
        c.innerHTML = selElmnt.options[j].innerHTML;
        c.addEventListener("click", function (e) {
          /*when an item is clicked, update the original select box,
          and the selected item:*/
          var y, i, k, s, h;
          s = this.parentNode.parentNode.querySelector('select');
          h = this.parentNode.previousSibling;

          for (i = 0; i < s.length; i++) {
            if (s.options[i].innerHTML == this.innerHTML) {
              s.selectedIndex = i;
              h.innerHTML = "<span>" + this.innerHTML + "</span>";
              y = this.parentNode.getElementsByClassName("same-as-selected");
              for (k = 0; k < y.length; k++) {
                y[k].removeAttribute("class");
              }
              this.setAttribute("class", "same-as-selected");
              break;
            }
          }
          h.click();
        });
        b.appendChild(c);
      }
      x.appendChild(b);
      a.addEventListener("click", function (e) {
        /*when the select box is clicked, close any other select boxes,
        and open/close the current select box:*/
        e.stopPropagation();
        closeAllSelect(this);
        this.nextSibling.classList.toggle("select-hide");
        this.classList.toggle("select-arrow-active");
      });
    }

    function closeAllSelect(elmnt) {
      /*a function that will close all select boxes in the document,
      except the current select box:*/
      var x,
          y,
          i,
          arrNo = [];
      x = document.getElementsByClassName("select-items");
      y = document.getElementsByClassName("select-selected");
      for (i = 0; i < y.length; i++) {
        if (elmnt == y[i]) {
          arrNo.push(i);
        } else {
          y[i].classList.remove("select-arrow-active");
        }
      }
      for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
          x[i].classList.add("select-hide");
        }
      }
    }

    /*if the user clicks anywhere outside the select box,
    then close all select boxes:*/
    document.addEventListener("click", closeAllSelect);
  }

  return {
    init: init
  };
}();
'use strict';

var MasonryBlocks = function () {

    var $masonryContainer, mason;

    function init() {
        $masonryContainer = $('.masonry-block');

        if (!$masonryContainer.length) return;
        registerMediaQueries();
    }

    function layout() {
        if (typeof Masonry === 'undefined') {
            var err = 'Missing Masonry. Aborting layout';
            console.error(err);
            return new Error(err);
        }
        var options = {
            transitionDuration: 0,
            columnWidth: $masonryContainer.hasClass('third') ? '.third-width' : '.half-width',
            percentPosition: true
        };
        mason = new Masonry($masonryContainer[0], options);
        mason.layout();
        setTimeout(function () {
            mason.layout();
            if ($masonryContainer[0].offsetHeight === 0) {
                $masonryContainer[0].style.position = 'relative';
                $masonryContainer[0].style.height = 'auto';

                for (var a = 0; a < $masonryContainer[0].children.length; a++) {
                    var child = $masonryContainer[0].children[a];

                    if (child.classList.contains('three-columns-text-block') > -1) {
                        child.style.position = 'relative';
                    }
                }
            }
        }, 1000);
    }

    function registerMediaQueries() {
        enquire.register('screen and (min-width: 768px)', {
            match: function match() {
                layout();
            },
            unmatch: function unmatch() {
                if (typeof mason !== 'undefined') mason.destroy();
                $('section.body').attr('style', '');
            }
        });
    }

    return {
        init: init
    };
}();
'use strict';

var Navigation = function () {

    var $html, $body;
    var $navContainer, $heroNavigation, $heroNavigationLinks, $logoMenuHomepage, $homeContent, $heroVideo, $logoMenu, $menuButton;

    function init() {
        $html = $('html');
        $body = $('body');
        $navContainer = $('.site-navigation');
        $homeContent = $('.home-content');
        $heroVideo = $('.body video');
        $logoMenu = $('.logo-menu');
        $menuButton = $('.menu-button');
        $logoMenuHomepage = $('.logo-menu-homepage');
        $heroNavigation = $('.hero-navigation-background');
        $heroNavigationLinks = $('.hero-navigation-list li a');

        registerEvents();
        registerMediaQueries();
    }

    function toggleMenu() {
        var scrollTop = window.pageYOffset;
        requestAnimationFrame(function () {
            $html.toggleClass('navigation-active');
            if ($html.hasClass('navigation-active')) {
                $navContainer[0].scrollTop = 0;
                if (window.scrollBarWidth) {
                    $body.css({ 'padding-right': window.scrollBarWidth });
                    $logoMenu.css({ 'right': window.scrollBarWidth });
                    $menuButton.css({ 'transform': 'translateX(-' + window.scrollBarWidth + 'px)' });
                    // Prevent scroll jump
                    window.scrollTo(0, scrollTop);
                }
            } else {
                window.scrollBarWidth && $body.css({ 'padding-right': 0 });
                $logoMenu.css({ 'right': 0 });
                $menuButton.css({ 'transform': 'none' });
            }
        });
    }

    function isHomeContent() {
        return $homeContent.length;
    }

    function showProperLogo() {
        if (isHomeContent()) {
            var screenWidth = $(window).width();
            if (screenWidth >= 992) {
                $logoMenuHomepage.css('display', 'block');
            } else {
                $logoMenuHomepage.css('display', 'none');
            }
        }
    }

    function setMenuPosition() {
        requestAnimationFrame(function () {
            var homeContentTop = $homeContent[0].getBoundingClientRect().top;
            var heroVideoBottom = $heroVideo[0].getBoundingClientRect().bottom;

            if (heroVideoBottom <= 74) {
                if (!$heroNavigation.hasClass('hide')) {
                    $heroNavigation.addClass('hide');
                }
            } else {
                if ($heroNavigation.hasClass('hide')) {
                    $heroNavigation.removeClass('hide');
                }
            }

            if (-homeContentTop >= 0) {
                if (!$logoMenuHomepage.hasClass('fixed')) {
                    $logoMenuHomepage.addClass('fixed');
                }
            } else {
                if ($logoMenuHomepage.hasClass('fixed')) {
                    $logoMenuHomepage.removeClass('fixed');
                }
            }
        });
    }

    function registerEvents() {
        $body.on('click.toggleMenu', '.menu-button', toggleMenu);
        $(window).on('resize', showProperLogo);
        if (isHomeContent()) {
            $(window).on('resize', setMenuPosition);
        }
    }

    function registerMediaQueries() {
        enquire.register('screen and (min-width: 768px)', {
            // SM and up
            match: function match() {
                if (isHomeContent()) {
                    $(window).on('load', setMenuPosition);
                    $(window).on('scroll', setMenuPosition);
                }
                $(window).on('load', showProperLogo);
                $heroNavigationLinks.on('click', function (event) {
                    event.stopPropagation();
                });
            },
            // up to, excluding SM
            unmatch: function unmatch() {
                $(window).off('scroll', setMenuPosition);
                $heroNavigationLinks.off('click');
                $(window).off('load', setMenuPosition);
                $(window).off('scroll', setMenuPosition);
            }
        });
    }

    return {
        init: init
    };
}();
'use strict';

var Newsletter = function () {

    var $form;

    function init() {
        $form = $('form.mailchimp').first();
        if ($form.length > 0) {
            $form.submit(function (event) {
                event.preventDefault();
                ajaxSendForm($form);
            });
            $('input.email', $form).on('keyup keypress blur change', function () {
                $form.toggleClass('empty', $(this).val() === '');
            });
        }
    }

    function ajaxSendForm() {
        $form.addClass('loading');
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $form.serialize(),
            cache: false,
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            error: function error() {
                alert('Could not connect to the registration server. Please try again later.');
            },
            success: function success(data) {
                $form.removeClass('loading error success');
                if (data.result === 'success') {
                    $form.addClass('success');
                    $('.message', $form).html('Thank you for subscribing.');
                    $('.email', $form).val('');
                } else {
                    $form.addClass('error');
                    $('.message', $form).html(data.msg.substring(4));
                }
            }
        });
    }

    return {
        init: init
    };
}();
'use strict';

var Opportunity = function () {

    var $body, $masonryBlocks;

    function init() {
        $body = $('body');
        $masonryBlocks = $('.masonry-block');
        registerEvents();
    }

    function discloseOpportunity(event) {
        event.preventDefault();

        $(this).parent().parent().toggleClass('open');
        $masonryBlocks.masonry({ transitionDuration: 0 });
    }

    function registerEvents() {
        $body.on('click.discloseOpportunity', '.opportunity-block a.header', discloseOpportunity);
    }

    return {
        init: init
    };
}();
'use strict';

var Slides = function () {

    function init() {
        if ($('.gallery-block.-multiple').length > 0) {
            $('.gallery-block.-multiple .slides').slick({
                infinite: true,
                dots: true,
                slidesToShow: 1
            });
        }
    }
    return {

        init: init
    };
}();
'use strict';

var ThumbAnim = function () {

  function init() {
    $(window).on('scroll', $.throttle(250, reveal));
    reveal();
  }

  function reveal() {
    var windowHeight = $(window).height();
    $('.reveal:not(.revealed)').each(function () {
      var objPos = this.getBoundingClientRect();
      var deltaW = windowHeight * 0.1;
      if (objPos.top < windowHeight - deltaW && objPos.bottom > deltaW) {
        $(this).addClass('revealed');

        var $player_wrapper, $player, $iframe;

        $player_wrapper = $(this).find('.work-item-block__player');
        $iframe = $player_wrapper.find('.handstick');

        if ($iframe.length) {
          setTimeout(function () {
            $player = new Vimeo.Player($iframe[0], {
              width: 640,
              responsive: true
            });
          }, 500);

          $(this).find('.work-item-block__player-image').on('click', function (e) {
            var $image = $(this);
            $player.play();
            $player.on('play', function () {
              $image.css('opacity', 0);
              setTimeout(function () {
                $player_wrapper.css('z-index', 2);
              }, 500);
            });
          });
        }
      }
    });
    $('.reveal.revealed').each(function () {
      var objPos = this.getBoundingClientRect();
      if (objPos.top > windowHeight) {
        $(this).removeClass('revealed');
      }
    });
  }

  return {
    init: init
  };
}();
'use strict';

var ThumbSlideshow = function () {
    var TIME_PER_SLIDE = 1000;
    var TRANSITION_DURATION = 500;
    var slideInterval = null;
    var $window = $(window);

    $.fn.queueAddClass = function (className) {
        this.queue('fx', function (next) {
            $(this).addClass(className);
            next();
        });
        return this;
    };

    $.fn.queueRemoveClass = function (className) {
        this.queue('fx', function (next) {
            $(this).removeClass(className);
            next();
        });
        return this;
    };

    var playSlideshow = function playSlideshow(images, totalSlides) {
        for (var i = 0; i < totalSlides; i++) {
            var extraDelay = i === totalSlides - 1 ? 0 : TRANSITION_DURATION;
            $(images[i]).delay(i * TIME_PER_SLIDE).queueAddClass('show').delay(TIME_PER_SLIDE + extraDelay).queueRemoveClass('show');
        }
    };

    var startSlideshow = function startSlideshow(images) {
        var totalSlides = images.length;

        if (totalSlides) {
            playSlideshow(images, totalSlides);
            slideInterval = setInterval(function () {
                playSlideshow(images, totalSlides);
            }, totalSlides * TIME_PER_SLIDE + TIME_PER_SLIDE);
        }
    };

    var stopSlideshow = function stopSlideshow(images) {
        $(images).stop();
        $(images).removeClass('show');
        clearInterval(slideInterval);
    };

    var getThumbnails = function getThumbnails() {
        return $('.thumbnail .href-wrapper');
    };

    var addThumbnailsEvents = function addThumbnailsEvents() {
        getThumbnails().each(function (index, el) {
            var slideshowImages = $(el).find('.slideshow-image');
            if (slideshowImages.length) {
                $(el).on('mouseover', function () {
                    return startSlideshow(slideshowImages);
                });
                $(el).on('mouseout', function () {
                    return stopSlideshow(slideshowImages);
                });
            }
        });
    };

    var removeThumbnailsEvents = function removeThumbnailsEvents() {
        getThumbnails().each(function (index, el) {
            var slideshowImages = $(el).find('.slideshow-image');
            if (slideshowImages.length) {
                $(el).off();
                $(el).off();
            }
        });
    };

    var registerEvents = function registerEvents() {
        $window.on('load', addThumbnailsEvents);
        $window.on('loadContent', addThumbnailsEvents);
        $window.on('unloadContent', removeThumbnailsEvents);
    };

    var init = function init() {
        registerEvents();
    };

    return {
        init: init
    };
}();
'use strict';

var WorkIndex = function () {
    var $window, $body, $pageHeader, $navigationContainer, $siteContent, $dummyNavigationContainer, $navigation, $subNavigation, $pageContent, $layoutParent, mason;
    var activeFiltersClass = 'filters-active';
    var activeSubFiltersClass = 'subfilters-active';
    var activeFilterClass = 'active';

    var wAreas = window.w_areas;

    var lastScrollTop = 0;
    var initScrollTop = 0;
    var scrollDir = 0;

    function init() {
        $window = $(window);
        $body = $('body');
        $pageContent = $body.find('.home-content');
        $pageHeader = $pageContent.find('.page-header');
        $navigationContainer = $('.logo-menu');
        $siteContent = $('.site-content .page-content');
        $navigation = $pageHeader.find('.work-navigation');
        $subNavigation = $pageHeader.find('.work-subnavigation');
        $layoutParent = $pageContent.children('.body');
        setNavigationState();
        setSubNavigationState();
        registerMediaQueries();
        registerEvents();
        addRandomDelayClasses($layoutParent.children());
        setTimeout(enableHeaderTransition, 500);

        $window.on('scroll', fixNavigation);
        $window.trigger('scroll');
    }

    function getCategoryPath() {
        var index = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;

        var pathname = getPathnameOnly(window.location.href);
        var parts = pathname.replace(/^\/|\/$/, '').split('/');
        var matches = pathname.match(new RegExp('/(' + wAreas.join('|') + ')/', 'gi'));
        if (matches) {
            return matches[0] + parts[index + 1];
        }
        if (parts[index]) {
            return '/' + parts[index];
        }
        return;
    }

    function fixNavigation() {
        var navigationContainerHeight = $navigationContainer.outerHeight();
        var siteContentTop = $siteContent[0].getBoundingClientRect().top;

        if (siteContentTop - navigationContainerHeight <= 0) {
            if (!$navigationContainer.hasClass('fixed')) {
                $navigationContainer.addClass('fixed');
                $navigationContainer.addClass('initialized');
            }
        }
        if (siteContentTop - navigationContainerHeight >= 0) {
            if ($navigationContainer.hasClass('fixed')) {
                $navigationContainer.removeClass('fixed');
            }
        }
    }

    function setHiddenNavigationHeight() {
        var height = $navigationContainer.outerHeight();
        $dummyNavigationContainer.css('height', height + 'px');
    }

    function showHideNavigation() {
        var scrollY = window.pageYOffset;
        var dummyNavigationHeight = $navigationContainer.outerHeight();
        var newScrollDir = scrollY > lastScrollTop;
        lastScrollTop = scrollY;
        if (newScrollDir != scrollDir) {
            initScrollTop = scrollY;
            scrollDir = newScrollDir;
        }
        var delta = scrollY - initScrollTop;
        if (delta + dummyNavigationHeight > 0 && $navigationContainer.hasClass('fixed')) {
            $navigationContainer.addClass('stashed-away');
        }
        // Show again if top of the page or enough movement
        if (delta < -100 || scrollY < 200) {
            $navigationContainer.removeClass('stashed-away');
        }
    }

    function enableHeaderTransition() {
        // $pageHeader.css('transition-duration', '200ms');
        $pageHeader.addClass('enable-transition');
    }

    function getActiveCategory() {
        /*
        Checks if there is something else than /work in the url.
        Returns and object with:
            categoryHandle: '' for /work and 'vr' for /work/vr
            categoryElement: the corresponding element in the nav
            subCategoryHandle: '' for /work/animation and '2d' for /work/animation/2d
            subCategoryElement: the corresponding element in the nav
        */

        var pathname = window.location.pathname;
        var parts = pathname.replace(/^\/|\/$/, '').split('/');
        var categoryElement, categoryHandle, subCategoryElement, subCategoryHandle;
        if (parts) {
            if (parts[0]) {
                categoryHandle = parts[0];
            }
            if (parts[1]) {
                subCategoryHandle = parts[1];
            }
        } else {
            return null;
        }
        // Find the link that matches our pathname
        // The pathname can be:
        // - /work/category?
        // - /na/work/category?
        categoryElement = $navigation.find('a').toArray().map($).filter(function ($el) {
            var elPathname = getPathnameOnly($el.attr('href'));
            var category = getCategoryPath();
            return elPathname === category;
        })[0];

        // Find the link that matches our pathname
        // The pathname can be:
        // - /work/category/subcategory?
        // - /na/work/category/subcategory?
        $subNavigation = $pageHeader.find('.work-subnavigation');
        subCategoryElement = $subNavigation.find('a').toArray().map($).filter(function ($el) {
            var elPathname = getPathnameOnly($el.attr('href')).replace(getCategoryPath(), '');
            var subcategory = getCategoryPath(1);
            return elPathname === subcategory;
        })[0];

        return {
            categoryHandle: categoryHandle,
            categoryElement: categoryElement,
            subCategoryHandle: subCategoryHandle,
            subCategoryElement: subCategoryElement
        };
    }

    function setNavigationState() {
        var activeCategory = getActiveCategory().categoryElement;
        if (!activeCategory) {
            $pageContent.removeClass(activeFiltersClass);
            $navigation.find('a').removeClass(activeFilterClass);
        } else {
            $pageContent.addClass(activeFiltersClass);
            $navigation.find('a').removeClass(activeFilterClass);
            activeCategory.addClass(activeFilterClass);
        }
    }

    function setSubNavigationState() {
        var activeSubCategory = getActiveCategory().subCategoryElement;
        if (!activeSubCategory) {
            $pageContent.removeClass(activeSubFiltersClass);
            $subNavigation.find('a').removeClass(activeFilterClass);
        } else {
            $pageContent.addClass(activeSubFiltersClass);
            $subNavigation.find('a').removeClass(activeFilterClass);
            activeSubCategory.addClass(activeFilterClass);
        }
    }

    function getPathnameOnly(url) {
        /*
        From a given url, match only the path, without host, query or hash
        https://www.google.co.uk/search?q=ibm+watson&oq=ibm+watson&aqs=chrome..69i57j0l5.5285j0j7&sourceid=chrome&ie=UTF-8
        -> /search
        */
        var re = new RegExp('^(?:https?:)?//.+?(/.*?)(?:/|/?[#?]+.*)?$');
        var matches = url.match(re);
        if (matches && matches.length > 1) return matches[1];
        return url;
    }

    function addRandomDelayClasses($els) {
        var delays = ['delay-1', 'delay-2', 'delay-3', 'delay-4'];
        // Math.floor(Math.random()*myArray.length)
        $els.toArray().map($).forEach(function ($el) {
            $el.addClass(delays[Math.floor(Math.random() * delays.length)]);
        });
    }

    function hasCategoryChanged(currentPath, previousPath) {
        var currentPathnameCategory = currentPath.replace(/^\/|\/$/, '').split('/')[0];
        var previousPathnameCategory = previousPath.replace(/^\/|\/$/, '').split('/')[0];

        return currentPathnameCategory !== previousPathnameCategory;
    }

    function loadCategory(categoryURL, previousURL) {
        categoryURL = categoryURL.replace(new RegExp('/(' + wAreas.join('|') + ')/', 'gi'), '');
        var contentPath = categoryURL === '/' ? '/' : '/?pagename=' + categoryURL;
        var $oldDescription = $body.find('.home-content > .description');
        var $oldSubnavigation = $body.find('.home-content .work-subnavigation');
        var $oldBlocks = $layoutParent.children();
        addRandomDelayClasses($oldBlocks);
        var categoryHasChanged = hasCategoryChanged(categoryURL, previousURL);
        $oldBlocks.css({ opacity: 0 });
        $oldDescription.css({ opacity: 0 });

        if (categoryHasChanged) {
            $oldSubnavigation.css({ opacity: 0 });
        }

        $.get(contentPath, function () {
            $window.trigger('unloadContent');
        }).done(function (html) {
            var $html = $(html);
            var $newDescription = $html.find('.home-content > .description');
            var $newSubnavigation = $html.find('.home-content .work-subnavigation');
            var $newBlocks = $html.find('.page-content > .body').children();

            // Prepare
            $newBlocks.css({ opacity: 0 });
            $newDescription.css({ opacity: 0 });
            if (categoryHasChanged) {
                $newSubnavigation.css({ opacity: 0 });
            }

            addRandomDelayClasses($newBlocks);

            // Append & replace
            $oldDescription.replaceWith($newDescription);
            if (categoryHasChanged) {
                $oldSubnavigation.replaceWith($newSubnavigation);
            }
            setSubNavigationState();
            setHiddenNavigationHeight();

            requestAnimationFrame(function () {
                $layoutParent.empty();
                $layoutParent.append($newBlocks);

                // Reveal
                requestAnimationFrame(function () {
                    $newBlocks.css({ opacity: 1 });
                    $newDescription.css({ opacity: 1 });
                    if (categoryHasChanged) {
                        $newSubnavigation.css({ opacity: 1 });
                    }
                    $window.trigger('scroll');
                    $window.trigger('loadContent');

                    if (mason) {
                        // Layout
                        requestAnimationFrame(function () {
                            layout($layoutParent);
                            setTimeout(function () {
                                // This is important
                                mason.reloadItems();
                                $(window).trigger('scroll');
                            }, 100);
                        });
                    }
                });
            });
        }).fail(function () {
            $oldBlocks.css({ opacity: 1 });
            $oldDescription.css({ opacity: 1 });
            $oldSubnavigation.css({ opacity: 1 });
            return new Error('Error loading'.categoryURL);
        });
    }

    function layout($layoutParent) {
        if (typeof Masonry === 'undefined') {
            var err = 'Missing Masonry. Aborting layout';
            return new Error(err);
        }
        var options = {
            transitionDuration: 0,
            columnWidth: '.half-width'
        };
        mason = new Masonry($layoutParent[0], options);
    }

    function registerEvents() {
        // Load category
        $body.on('click', '.work-navigation a', function (event) {
            event.preventDefault();
            var pathname = getPathnameOnly($(this).attr('href')).replace(/\/$/, '');
            var currentPathname = getPathnameOnly(window.location.href).replace(/\/$/, '');
            if (pathname === currentPathname) {
                /* Clear filters */
                pathname = '/';
            }
            history.pushState(null, '', pathname);
            loadCategory(pathname, currentPathname);
            setNavigationState();
        });
        $body.on('click', '.work-subnavigation a', function (event) {
            event.preventDefault();
            var pathname = getPathnameOnly($(this).attr('href')).replace(/\/$/, '');
            var currentPathname = getPathnameOnly(window.location.href).replace(/\/$/, '');
            if (pathname === currentPathname) {
                /* Clear subfilters */
                pathname = '' + getCategoryPath();
            }

            history.pushState(null, '', pathname);
            loadCategory(pathname, currentPathname);
            setNavigationState();
        });
        // Simple routing
        $window.on('popstate', function () {
            var pathname = getPathnameOnly(window.location.href);
            loadCategory(pathname);
            setNavigationState();
        });
        // $window.on('load', setHiddenNavigationHeight);
        // $window.on('resize', setHiddenNavigationHeight);
    }

    function registerMediaQueries() {
        enquire.register('screen and (min-width: 768px)', {
            match: function match() {
                requestAnimationFrame(function () {
                    layout($layoutParent);
                });
                // $window.on('scroll.showHideNavigation', $.throttle(200, showHideNavigation));
            },
            unmatch: function unmatch() {
                if (typeof mason !== 'undefined') mason.destroy();
                $('section.body').attr('style', '');
                // $window.off('scroll.showHideNavigation');
            }
        });
    }

    return {
        init: init
    };
}();
'use strict';

var Home = function () {

    var $window, $html, $body, $pageContent, $projectBlocks, $projectHeaders, $heroBlock, $heroBlockVideo, $faderReference, $markerReference, $pusherReference, $straplines, $storyStraplines, $breakers, $titles, $homeContent, $workLink;

    var breakerSelectors = ['.home-featured-strapline-block', '.home-news-break', '.newsletter-block', '.end-block'];

    var layout = {
        fader: {},
        marker: {},
        pusher: {},
        straplines: [
            /*
            {
                el: …,
                top: …,
                bottom: …,
                children: [$…]
            }
            */
        ],
        storyStraplines: [],
        breakers: [],
        titles: [],
        hero: {},
        video: {},
        content: {}
    };

    function init() {
        $window = $(window);
        $html = $('html');
        $body = $('body');
        $homeContent = $('.home-content');
        $pageContent = $body;
        if (!$pageContent.length) return;

        $faderReference = $('.util-fader');
        $markerReference = $('.util-marker');
        $pusherReference = $('.util-pusher');
        $straplines = $pageContent.find('.home-featured-strapline-block');
        $storyStraplines = $pageContent.find('.story-strapline-block');
        $titles = $straplines.find('.title');
        $workLink = $('.work-link a');

        $breakers = $pageContent.find(breakerSelectors.join(','));
        $projectBlocks = $pageContent.find('.home-featured-project-block');
        $projectHeaders = $projectBlocks.find('.header:not(.story-header)');
        $heroBlock = $('.home-header-block');
        $heroBlockVideo = $heroBlock.find('video');

        enableInlineVideoPlayback();

        updateLayoutCache();

        registerMediaQueries();
        registerEvents();

        $window.trigger('scroll');
    }

    function isHomeContent() {
        return $homeContent.length;
    }

    function easeOutQuad(t) {
        return t * (2 - t);
    }

    function enableInlineVideoPlayback() {
        if (!$('.home-header-block video').get(0)) return;
        enableInlineVideo($('.home-header-block video').get(0), { iPad: true });
    }

    function updateTitlesAlpha() {
        var fadeMinY = 70;
        var fadeMaxY = 250;
        $projectHeaders.each(function () {
            var pos = this.getBoundingClientRect();
            this.style.opacity = easeOutQuad(clampedRange(fadeMinY, fadeMaxY, pos.top)) + '';
        });
    }

    function resetTitlesAlpha() {
        $projectHeaders.each(function () {
            this.style.opacity = '1';
        });
    }

    function pauseHeroVideoOnScroll() {
        var scrollY = window.pageYOffset;
        var videoEl = $heroBlockVideo.get(0);
        if (!videoEl) return;

        var isScrolledOut = layout.video.bottom - scrollY > 0;

        if (!isScrolledOut && !videoEl.paused) {
            videoEl.pause();
        }
        if (isScrolledOut && videoEl.paused) {
            videoEl.play();
        }
    }

    function scrollToMainContent() {
        var $pageContentEl = $('article.page-content');
        var navigationContainerHeight = $('.logo-menu').outerHeight();
        var scrollVal = $pageContentEl.offset().top - navigationContainerHeight + 1;
        $('html,body').animate({
            scrollTop: scrollVal
        }, 500, 'swing');
    }

    function updateLayoutCache() {
        if ($heroBlock[0]) {
            layout.hero.top = $heroBlock[0].offsetTop;
            layout.hero.bottom = layout.hero.top + $heroBlock[0].offsetHeight;
        }

        if ($heroBlockVideo[0]) {
            layout.video.top = $heroBlockVideo[0].offsetTop;
            layout.video.bottom = layout.video.top + $heroBlockVideo[0].offsetHeight;
        }

        if ($faderReference[0]) {

            layout.fader.top = $faderReference[0].offsetTop;
            layout.fader.bottom = layout.fader.top + $faderReference[0].offsetHeight;
        }

        if ($pusherReference[0]) {
            layout.pusher.top = $pusherReference[0].offsetTop;
            layout.pusher.bottom = layout.pusher.top + $pusherReference[0].offsetHeight;
        }

        if ($markerReference[0]) {
            layout.marker.top = $markerReference[0].offsetTop;
        }
        if ($homeContent[0]) {
            layout.content.top = $homeContent[0].offsetTop;
        }

        function iter($el) {
            var el = $el[0];
            var offset = getAbsoluteOffset(el);
            return {
                top: offset.top,
                bottom: offset.top + el.offsetHeight,
                el: $el
            };
        }
        layout.straplines = $straplines.toArray().map($).map(iter);
        layout.storyStraplines = $storyStraplines.toArray().map($).map(iter);
        layout.straplinesChildren = $straplines.find('.title-word').toArray().map($).map(iter);
        layout.storyStraplinesChildren = $storyStraplines.find('.title-word').toArray().map($).map(iter);
        layout.breakers = $breakers.toArray().map($).map(iter);
        layout.titles = $titles.toArray().map($).map(iter);

        layout.straplines.forEach(function (obj) {
            obj.children = obj.el.find('.title-word');
        });

        layout.storyStraplines.forEach(function (obj) {
            obj.children = obj.el.find('.title-word');
        });
    }

    function clampedRange(min, max, value) {
        var clampedVal = Math.max(min, Math.min(max, value));
        return (clampedVal - min) / (max - min);
    }

    function fadeStraplines() {
        var scrollY = window.pageYOffset;
        var faderTop = scrollY + layout.fader.top;
        var faderBottom = scrollY + layout.fader.bottom;

        /*
        Strapline behaviour.
        Vary opacity from 1 to 0.
        */
        layout.straplinesChildren.forEach(function (obj) {
            var posRange = clampedRange(faderTop, faderBottom, obj.top);
            obj.el[0].style.opacity = posRange + '';
        });
    }

    function fadeStoryStraplines() {
        var scrollY = window.pageYOffset;
        var offset;

        if ($window.width() >= 1200) {
            offset = 120;
        } else if ($window.width() >= 992) {
            offset = 90;
        } else {
            offset = 60;
        }

        var faderTop = scrollY + layout.fader.top + offset;
        var faderBottom = scrollY + layout.fader.bottom + offset;

        layout.storyStraplinesChildren.forEach(function (obj) {
            var posRange = clampedRange(faderTop, faderBottom, obj.top);
            obj.el[0].style.opacity = posRange + '';
        });
    }

    function trackScroll() {
        var scrollY = window.pageYOffset;
        var markerScrollTop = scrollY + layout.marker.top;
        var pusherTop = scrollY + layout.pusher.top;
        var pusherBottom = scrollY + layout.pusher.bottom;
        var pusherHeight = layout.pusher.bottom - layout.pusher.top;

        /*
        Strapline Title behaviour
        Sticks when it reaches the marker.
        */
        var stickyTitle = false;
        layout.titles.forEach(function (obj) {
            // Prepare stuff
            var nextBreaker = null;
            var style = obj.el[0].style;
            // Find the next "breaker"
            layout.breakers.forEach(function (breaker) {
                if (nextBreaker) return;
                if (breaker.top > obj.top) {
                    nextBreaker = breaker;
                }
            });
            if (nextBreaker == null) return;
            var breakerPosRange = clampedRange(pusherTop, pusherBottom, nextBreaker.top);

            // Place things
            style.position = 'absolute';
            style.top = 'auto';
            obj.el.removeClass('active');

            // While the title is above the marker, leave it in place
            if (markerScrollTop < obj.top) {
                style.transform = 'translate3d(0,0,0)';
                return;
            }

            // With the title below the marker, stamp the title in place
            style.transform = 'translate3d(0,' + (nextBreaker.top - obj.top - pusherHeight + layout.marker.top) + 'px,0)';

            // While the breaker is out of range keep the title fixed
            if (breakerPosRange >= 1) {
                style.position = 'fixed';
                style.top = layout.marker.top + 'px';
                style.transform = 'translate3d(0,0,0)';
                obj.el.addClass('active');
                stickyTitle = true;
            }
        });

        $body.toggleClass('sticky-title', stickyTitle);
    }

    function getAbsoluteOffset(el, offsetParent) {
        var _x = 0;
        var _y = 0;
        while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
            _x += el.offsetLeft;
            _y += el.offsetTop;
            if (offsetParent && el.offsetParent === offsetParent) {
                break;
            }
            el = el.offsetParent;
        }
        return { top: _y, left: _x };
    }

    function removeSticky() {
        layout.titles.forEach(function (obj) {
            var style = obj.el[0].style;
            style.position = 'absolute';
            style.top = 'auto';
            obj.el.removeClass('active');
        });
        layout.straplinesChildren.forEach(function (obj) {
            obj.el[0].style.opacity = 1;
        });
    }

    function handleWorkLink(event) {
        if (isHomeContent()) {
            event.preventDefault();
            if ($html.hasClass('navigation-active')) {
                $html.removeClass('navigation-active');
            }
            // setTimeout(function () {
            //     scrollToMainContent();
            // }, 300);
        }
        if ($body.css('padding-right')) {
            $body.css({ 'padding-right': 0 });
        };
    }

    function registerEvents() {
        $window.on('scroll', function () {
            requestAnimationFrame(pauseHeroVideoOnScroll);
        });

        // $window.on('load',function () {
        //     const { pathname, hash } = document.location;
        //
        //     if (hash === '#work' || ($homeContent.length && pathname !== '/')) {
        //         scrollToMainContent();
        //     }
        // });

        $workLink.on('click', handleWorkLink);

        $window.on('scroll', function () {
            requestAnimationFrame(fadeStraplines);
            requestAnimationFrame(fadeStoryStraplines);
        });

        //$body.on('click', '.home-header-block', scrollToMainContent);

        $window.on('resize', $.debounce(250, updateLayoutCache));
    }

    function registerMediaQueries() {
        enquire.register('screen and (min-width: 900px)', {
            // SM and up
            match: function match() {
                $window.on('scroll.trackScroll', trackScroll);
                $window.on('scroll.updateTitlesAlpha', function () {
                    requestAnimationFrame(updateTitlesAlpha);
                });
            },
            // up to, excluding SM
            unmatch: function unmatch() {
                $window.off('scroll.trackScroll');
                $window.off('scroll.updateTitlesAlpha');
                resetTitlesAlpha();
                removeSticky();
            }
        });
    }

    return {
        init: init
    };
}();
'use strict';

var TalentIndex = function () {

    // var $menuButton;
    var $body, $talentBackgroundEls;
    var hideAllBackgroundsTimeout;

    function init() {
        if (!$('.talent-archive-content')) return false;
        $body = $('body');
        $talentBackgroundEls = $('.talent-background');
        registerEvents();
    }

    function showBackground() {
        clearTimeout(hideAllBackgroundsTimeout);
        $body.addClass('hover');
        $talentBackgroundEls.removeClass('prev');
        $talentBackgroundEls.filter('.show').removeClass('show').addClass('prev');
        $talentBackgroundEls.removeClass('show').filter('#bkg-' + $(this).attr('id')).addClass('show');
    }

    function hideAllBackgrounds() {
        hideAllBackgroundsTimeout = setTimeout(function () {
            $body.removeClass('hover');
        }, 1000);
    }

    function registerEvents() {
        $body.on('mouseenter touchstart', 'a.talent-link', function () {
            clearTimeout(hideAllBackgroundsTimeout);
        });
        $body.on('mouseenter touchstart', 'a.talent-link', $.debounce(100, showBackground));
        $body.on('mouseleave touchend', 'a.talent-link', hideAllBackgrounds);
    }

    return {
        init: init
    };
}();
'use strict';

var ToggleClass = function () {

    function init() {
        $('body').on('click', '[data-toggle-el]', function (event) {
            event.preventDefault();
            var toggleClass = $(this).data('toggle-class') || 'show';
            $($(this).data('toggle-el')).toggleClass(toggleClass);
            $(this).toggleClass('toggled');
        });
    }

    return {
        init: init
    };
}();
// if (console && console.log) {
//     // Vanity
//     console.log([
//         '%c',
//         '          Website by           ',
//         '                               ',
//         ' ▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲ ',
//         ' ▲    ▼▲▼▲▼▲▼▲▼ ▼▲▼▲▼▲▼▲▼    ▲ ',
//         ' ▼▲    ▼▲▼▲▼▲▼   ▼▲▼▲▼▲▼    ▲▼ ',
//         ' ▲▼▲    ▼▲▼▲▼     ▼▲▼▲▼    ▲▼▲ ',
//         ' ▼▲▼▲    ▼▲▼       ▼▲▼    ▲▼▲▼ ',
//         ' ▲▼▲▼▲    ▼    ▲    ▼    ▲▼▲▼▲ ',
//         ' ▼▲▼▲▼▲       ▲▼▲       ▲▼▲▼▲▼ ',
//         ' ▲▼▲▼▲▼▲     ▲▼▲▼▲     ▲▼▲▼▲▼▲ ',
//         ' ▼▲▼▲▼▲▼▲   ▲▼▲▼▲▼▲   ▲▼▲▼▲▼▲▼ ',
//         ' ▲▼▲▼▲▼▲▼▲ ▲▼▲▼▲▼▲▼▲ ▲▼▲▼▲▼▲▼▲ ',
//         ' ▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼ ',
//         '                               ',
//         ' The Workers    theworkers.net ',
//     ].join('\n'),
//     'color:white; background:#3076bb; font-size: 12px; font-family: Monaco, Menlo, Courier, Courier New, Andale Mono; line-height: 1;'
//     );
// }
"use strict";
'use strict';

// Bootstrap scripts


removeClass(document.documentElement, 'no-js');

$(function () {
    window.scrollBarWidth = getScrollbarWidth();
    setInterval(function () {
        requestAnimationFrame(function () {
            window.scrollBarWidth = getScrollbarWidth();
        });
    }, 1000);

    // object fit polyfill
    objectFitImages();

    Home.init();
    WorkIndex.init();
    TalentIndex.init();
    MasonryBlocks.init();
    LookingFor.init();

    // These should be initialised ad-hoc
    Navigation.init();
    Clock.init();
    Slides.init();
    ToggleClass.init();
    Opportunity.init();
    Newsletter.init();
    ThumbAnim.init();
    ThumbSlideshow.init();

    // Show breakpoint debug on localhost
    if (window.location.host.match(/localhost|nikko.local|duderino.local/i)) {
        $('#mq').css({ display: 'block' });
    }
});

function removeClass(el, className) {
    if (el.classList) {
        el.classList.remove(className);
    } else {
        el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
    }
}

function getScrollbarWidth() {
    var outer = document.createElement('div');
    outer.style.visibility = 'hidden';
    outer.style.width = '100px';
    document.body.appendChild(outer);

    var widthNoScroll = outer.offsetWidth;
    // force scrollbars
    outer.style.overflow = 'scroll';

    // add innerdiv
    var inner = document.createElement('div');
    inner.style.width = '100%';
    outer.appendChild(inner);

    var widthWithScroll = inner.offsetWidth;

    // remove divs
    outer.parentNode.removeChild(outer);

    return widthNoScroll - widthWithScroll;
}
//# sourceMappingURL=script.js.map
